# Contributing

First of, thank you for considering being a part of f4ratk.

This is a young project. Many processes have not been fledged out yet
and written guidelines are scarce. To avoid waste and the frustration
that comes with it, get in touch with the maintainers first. Create
a feature request or bug information at the [ISSUE TRACKER], start
a [PULL REQUEST] in an early stage or reach out directly, e.g.,
via [EMAIL]. This allows us to discuss your idea and to find out
how we can design and align it with the goals of this project best.

If you have questions, feel free to ask.

[ISSUE TRACKER]: https://codeberg.org/toroettg/f4ratk/issues
[PULL REQUEST]: https://codeberg.org/toroettg/f4ratk/pulls
[EMAIL]: mailto:dev@roettger-it.de
