##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from typing import Generator
from unittest.mock import Mock

from _pytest.pytester import LineMatcher
from click.testing import CliRunner
from pytest import fixture, mark
from pytest_mock import MockerFixture

from f4ratk.cli.commands import main
from f4ratk.domain import Region
from f4ratk.history import History


class TestHistoryCommand:
    @fixture(scope='function', name='mock')
    def mock_display_history(
        self, mocker: MockerFixture
    ) -> Generator[Mock, None, None]:
        mock = Mock()

        mock_history = Mock(spec_set=History, **{'display_history': mock})

        mocker.patch(
            'f4ratk.cli.commands.di', **{'__getitem__.return_value': mock_history}
        )

        yield mock

    @mark.parametrize(
        'region_arg, expected_region',
        (
            ('DEVELOPED', Region.DEVELOPED),
            ('developed', Region.DEVELOPED),
            ('Developed', Region.DEVELOPED),
            ('DEVELOPED-EX-US', Region.DEVELOPED_EX_US),
            ('developed-ex-us', Region.DEVELOPED_EX_US),
            ('Developed-Ex-Us', Region.DEVELOPED_EX_US),
            ('EMERGING', Region.EMERGING),
            ('emerging', Region.EMERGING),
            ('Emerging', Region.EMERGING),
            ('US', Region.US),
            ('us', Region.US),
            ('uS', Region.US),
            ('EU', Region.EU),
            ('eu', Region.EU),
            ('Eu', Region.EU),
        ),
    )
    def given_any_valid_region_argument_when_invoking_history_command_should_pass_region(  # noqa: E501
        self, region_arg: str, expected_region: Region, mock: Mock
    ):
        result = CliRunner().invoke(main, ('history', region_arg))

        assert result.exit_code == 0

        mock.assert_called_once_with(region=expected_region)


class TestHistoryCommandHelp:
    @fixture(scope='class', name='output')
    def given_help_option_when_invoked(self) -> Generator[str, None, None]:
        result = CliRunner().invoke(main, ('history', '--help'))
        assert result.exit_code == 0
        yield result.output

    def should_display_help_option(self, output: str):
        assert '--help' in output

    def should_display_history_type_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            r"Usage\:.*?history.*?"
            + r"\{DEVELOPED\|DEVELOPED-EX-US\|US\|EU\|EMERGING\}"
        )
