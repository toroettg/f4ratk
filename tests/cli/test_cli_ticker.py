##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from datetime import date
from typing import Generator
from unittest.mock import ANY, Mock

from _pytest.pytester import LineMatcher
from click.testing import CliRunner
from pytest import fixture, mark
from pytest_mock import MockerFixture

from f4ratk.cli.commands import main
from f4ratk.domain import AnalysisConfig, Currency, Frame, Frequency, ModelType, Region
from f4ratk.ticker.api import TickerAnalyzer
from f4ratk.ticker.reader import Stock


class TestTickerCommandHelp:
    @fixture(scope='class', name='output')
    def given_help_option_when_invoked(self) -> Generator[str, None, None]:
        result = CliRunner().invoke(main, ('ticker', '--help'))
        assert result.exit_code == 0
        yield result.output

    def should_display_help_option(self, output: str):
        assert '--help' in output

    def should_display_ticker_symbol_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            r"Usage\:.*?ticker.*?" + "SYMBOL"
        )

    def should_display_region_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            r"Usage\:.*?ticker.*?" + r"\{DEVELOPED\|DEVELOPED-EX-US\|US\|EU\|EMERGING\}"
        )

    def should_display_currency_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            (r"Usage\:.*?ticker.*?", r"\s+?\{USD\|EUR\}"), consecutive=True
        )

    def should_display_start_date_option(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            '.*?'.join(
                (
                    '',
                    r'\-\-start',
                    r'\[\%Y\-\%m\-\%d\]',
                    r'Start\ of\ period\ under\ review\.',
                )
            )
        )

    def should_display_end_date_option(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            '.*?'.join(
                (
                    '',
                    r'\-\-end',
                    r'\[\%Y\-\%m\-\%d\]',
                    r'End\ of\ period\ under\ review\.',
                )
            )
        )

    def should_display_frequency_option(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            (
                '.*?'.join(
                    (
                        '',
                        r'\-\-frequency',
                        r'\[DAILY|MONTHLY]',
                        'Conduct analysis with given sample frequency.',
                    )
                ),
                '.*?'.join(('', r'\[default\:\ MONTHLY\]')),
            )
        )


class TestTickerCommand:
    @fixture(scope='function', name='mock')
    def mock_analyze_stock(self, mocker: MockerFixture) -> Generator[Mock, None, None]:
        mock = mocker.patch(
            'f4ratk.infrastructure.TickerAnalyzerAdapter.analyze_ticker_symbol',
            spec_set=TickerAnalyzer,
        )
        yield mock

    def given_any_symbol_when_analyzing_should_pass_same_symbol(self, mock: Mock):
        CliRunner().invoke(main, ('ticker', 'USSC.L', 'US', 'USD'))

        mock.assert_called_once_with(
            stock=Stock(ticker_symbol='USSC.L', currency=ANY), analysis_config=ANY
        )

    def given_any_input_when_analyzing_should_use_default_models(self, mock: Mock):
        CliRunner().invoke(
            main, ('ticker', 'USSC.L', 'US', 'USD', '--start=2020-08-01')
        )

        mock.assert_called_once_with(
            stock=ANY,
            analysis_config=AnalysisConfig(
                region=ANY,
                frame=ANY,
                models=(ModelType.CAPM, ModelType.FF3, ModelType.FF5, ModelType.FF6),
            ),
        )

    def given_any_start_date_in_iso_when_analyzing_should_pass_start_date(
        self, mock: Mock
    ):
        CliRunner().invoke(
            main, ('ticker', 'USSC.L', 'US', 'USD', '--start=2020-08-01')
        )

        mock.assert_called_once_with(
            stock=Stock(ticker_symbol=ANY, currency=ANY),
            analysis_config=AnalysisConfig(
                region=ANY,
                frame=Frame(frequency=ANY, start=date(2020, 8, 1), end=None),
                models=ANY,
            ),
        )

    def given_any_end_date_in_iso_when_analyzing_should_pass_end_date(self, mock: Mock):
        CliRunner().invoke(main, ('ticker', 'USSC.L', 'US', 'USD', '--end=2020-08-31'))

        mock.assert_called_once_with(
            stock=Stock(ticker_symbol=ANY, currency=ANY),
            analysis_config=AnalysisConfig(
                region=ANY,
                frame=Frame(frequency=ANY, start=None, end=date(2020, 8, 31)),
                models=ANY,
            ),
        )

    @mark.parametrize(
        'region_arg, expected_region',
        (
            ('DEVELOPED', Region.DEVELOPED),
            ('developed', Region.DEVELOPED),
            ('Developed', Region.DEVELOPED),
            ('DEVELOPED-EX-US', Region.DEVELOPED_EX_US),
            ('developed-ex-us', Region.DEVELOPED_EX_US),
            ('Developed-Ex-Us', Region.DEVELOPED_EX_US),
            ('EMERGING', Region.EMERGING),
            ('emerging', Region.EMERGING),
            ('Emerging', Region.EMERGING),
            ('US', Region.US),
            ('us', Region.US),
            ('uS', Region.US),
            ('EU', Region.EU),
            ('eu', Region.EU),
            ('Eu', Region.EU),
        ),
    )
    def given_any_valid_region_case_insensitive_when_analyzing_should_pass_region_object(  # noqa: E501
        self, mock: Mock, region_arg: str, expected_region: Region
    ):
        CliRunner().invoke(main, ('ticker', 'USSC.L', region_arg, 'USD'))

        mock.assert_called_once_with(
            stock=Stock(ticker_symbol=ANY, currency=ANY),
            analysis_config=AnalysisConfig(
                region=expected_region, frame=ANY, models=ANY
            ),
        )

    @mark.parametrize(
        'currency_arg, expected_currency',
        (
            ('USD', Currency.USD),
            ('usd', Currency.USD),
            ('uSd', Currency.USD),
            ('EUR', Currency.EUR),
            ('eur', Currency.EUR),
            ('Eur', Currency.EUR),
        ),
    )
    def given_any_valid_currency_case_insensitive_when_analyzing_should_pass_currency_object(  # noqa: E501
        self, mock: Mock, currency_arg: str, expected_currency: Currency
    ):
        CliRunner().invoke(main, ('ticker', 'USSC.L', 'US', currency_arg))

        mock.assert_called_once_with(
            stock=Stock(ticker_symbol=ANY, currency=expected_currency),
            analysis_config=ANY,
        )

    @mark.parametrize(
        'frequency_arg, expected_frequency',
        (
            ('DAILY', Frequency.DAILY),
            ('daily', Frequency.DAILY),
            ('DailY', Frequency.DAILY),
            ('MONTHLY', Frequency.MONTHLY),
            ('monthly', Frequency.MONTHLY),
            ('MONTHLY', Frequency.MONTHLY),
        ),
    )
    def given_any_valid_frequency_case_insensitive_when_analyzing_should_pass_frequency_object(  # noqa: E501
        self, mock: Mock, frequency_arg: str, expected_frequency: Frequency
    ):
        CliRunner().invoke(
            main, ('ticker', 'USSC.L', 'US', 'USD', f"--frequency={frequency_arg}")
        )

        mock.assert_called_once_with(
            stock=ANY,
            analysis_config=AnalysisConfig(
                region=ANY,
                frame=Frame(frequency=expected_frequency, start=ANY, end=ANY),
                models=ANY,
            ),
        )
