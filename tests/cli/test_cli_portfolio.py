##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from pathlib import Path
from typing import Generator, Iterable
from unittest.mock import ANY, Mock

from _pytest.pytester import LineMatcher
from click.testing import CliRunner
from pytest import fixture
from pytest_mock import MockerFixture

from f4ratk.cli.commands import main
from f4ratk.portfolio.analyze import PortfolioRequest
from f4ratk.portfolio.api import PortfolioAnalyzer


def match_line(text: str, *parts: str):
    LineMatcher(text.splitlines()).re_match_lines('.*?'.join(parts))


def match_lines(text: str, *lines: Iterable[str]):

    regex = tuple('.*?'.join(parts) for parts in lines)
    LineMatcher(text.splitlines()).re_match_lines(regex, consecutive=True)


class TestPortfolioCommandHelp:
    @fixture(scope='class', name='output')
    def given_help_option_when_invoked(self) -> Generator[str, None, None]:
        result = CliRunner().invoke(main, ('portfolio', '--help'))
        assert result.exit_code == 0
        yield result.output

    def should_display_help_option(self, output: str):
        assert '--help' in output

    def should_display_file_path_argument(self, output: str):
        match_line(output, "Usage", "portfolio", "PATH")

    def should_display_portfolio_name_argument(self, output: str):
        match_lines(
            output,
            (
                r'\s+',
                r'\-\-name',
                r'Name\ of\ the\ portfolio\ to\ analyze\ in\ the\ file\ at\ the\ given\ path\.',  # noqa: E501
            ),
            (r'\[default\:\ the\ first\ portfolio\ of\ the\ file\]'),
        )


class TestPortfolioCommand:
    @fixture(scope='function', name='mock')
    def mock_analyze_portfolio(
        self, mocker: MockerFixture
    ) -> Generator[Mock, None, None]:
        mock = Mock()

        mock_analyzer = Mock(
            spec_set=PortfolioAnalyzer, **{'analyze_portfolio_file': mock}
        )
        mocker.patch(
            'f4ratk.cli.commands.di', **{'__getitem__.return_value': mock_analyzer}
        )

        yield mock

    def given_an_existing_file_when_analyzing_portfolio_should_pass_path_argument(
        self, mock: Mock, tmp_path: Path
    ):
        def given():
            tmp_path.joinpath('dummy.yml').touch()

        given()

        target_path = tmp_path.joinpath('dummy.yml')

        CliRunner().invoke(main, ('portfolio', str(target_path)))

        mock.assert_called_once_with(
            request=PortfolioRequest(path=target_path, name=ANY)
        )

    def given_an_existing_file_when_analyzing_named_portfolio_should_pass_name_argument(
        self, mock: Mock, tmp_path: Path
    ):
        def given():
            tmp_path.joinpath('dummy.yml').touch()

        given()

        target_path = tmp_path.joinpath('dummy.yml')

        CliRunner().invoke(
            main, ('portfolio', str(target_path), '--name=portfolioname')
        )

        mock.assert_called_once_with(
            request=PortfolioRequest(path=ANY, name='portfolioname')
        )
