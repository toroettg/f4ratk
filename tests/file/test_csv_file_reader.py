##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from pathlib import Path
from tempfile import NamedTemporaryFile

from numpy import dtype

from f4ratk.file.reader import CsvFileReader
from tests.conftest import CsvFileReaderFactory


def should_parse_isodate_and_returns_column():
    with NamedTemporaryFile(mode='w+', prefix='f4ratk_test_tmp_') as file:
        print(
            *('2020-11-18,"15.35"', '2020-11-17,14.33'), sep='\n', end='\n', file=file
        )

        file.flush()

        result = CsvFileReader(path=Path(file.name)).read()

    assert len(result) == 2
    assert result.index.dtype == dtype('datetime64[ns]')
    assert result.index.name == 'Dates'
    assert list(result.columns) == ['Returns']

    assert result['Returns']['2020-11-18'] == 15.35
    assert result['Returns']['2020-11-17'] == 14.33


def test_mock_contract(create_csv_file_reader: CsvFileReaderFactory):
    result = create_csv_file_reader(
        ('2020-11-18', 15.35),
        ('2020-11-17', 14.33),
    ).read()

    assert len(result) == 2
    assert result.index.dtype == dtype('datetime64[ns]')
    assert result.index.name == 'Dates'
    assert list(result.columns) == ['Returns']

    assert result['Returns']['2020-11-18'] == 15.35
    assert result['Returns']['2020-11-17'] == 14.33
