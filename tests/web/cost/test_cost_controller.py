##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from flask import Response
from flask.testing import FlaskClient
from pytest import approx, mark


def given_all_statements_when_received_should_calculate_and_report_costs(
    client: FlaskClient,
):
    body = {
        'totalExpenseRatio': 0.3,
        'expenses': 249596,
        'transactionCosts': 12944,
        'withholdingTax': 296366,
        'capitalGainsTax': 10000,
        'securitiesLendingIncome': 5000,
    }

    response: Response = client.post('/v0/costs', json=body)

    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'

    body = response.get_json()

    assert len(body) == 3
    assert body['increases']['totalExpenseRatio'] == 0.3
    assert body['increases']['withholdingTaxRatio'] == approx(0.356214, rel=1e-5)
    assert body['increases']['capitalGainsTaxRatio'] == approx(0.0120194, rel=1e-5)
    assert body['increases']['transactionCostsRatio'] == approx(0.015558, rel=1e-5)
    assert body['decreases']['securitiesLendingRatio'] == approx(0.0060097, rel=1e-5)
    assert body['totalCostsRatio'] == approx(0.677782, rel=1e-5)


@mark.parametrize(
    'expenses',
    (-249596, 0, 249596),
)
def given_minimal_statements_when_received_should_calculate_and_report_costs(
    client: FlaskClient, expenses: int
):
    body = {
        'totalExpenseRatio': 0.3,
        'expenses': expenses,
    }

    response: Response = client.post('/v0/costs', json=body)

    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'

    body = response.get_json()

    assert body['increases']['totalExpenseRatio'] == 0.3
    assert body['increases']['withholdingTaxRatio'] is None
    assert body['increases']['capitalGainsTaxRatio'] is None
    assert body['increases']['transactionCostsRatio'] is None
    assert body['decreases']['securitiesLendingRatio'] is None
    assert body['totalCostsRatio'] == 0.3


def given_0_expenses_and_additional_ratio_when_received_should_calculate_and_report_costs(  # noqa: E501
    client: FlaskClient,
):
    body = {
        'totalExpenseRatio': 0.3,
        'expenses': 0,
        'transactionCosts': 12944,
        'withholdingTax': 296366,
        'capitalGainsTax': 10000,
        'securitiesLendingIncome': 5000,
    }
    response: Response = client.post('/v0/costs', json=body)

    assert response.status_code == 200


def given_invalid_statements_when_TER_lte_0_received_should_calculate_and_report_costs(
    client: FlaskClient,
):
    body = {
        'totalExpenseRatio': 0,
        'expenses': 249596,
    }

    response: Response = client.post('/v0/costs', json=body)

    assert response.status_code == 400
    assert response.headers['Content-Type'] == 'application/json'

    body = response.get_json()

    assert len(body['errors']) == 1
    assert body['errors'][0]['code'] == 'INVALID'
    assert body['errors'][0]['source']['pointer'] == '/totalExpenseRatio'
    assert body['errors'][0]['detail'] == 'Must be greater than 0.'
