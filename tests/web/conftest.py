##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

import os
from json import dumps
from typing import Dict

from flask import Flask
from flask.testing import FlaskClient
from pytest import fixture
from pytest_mock import MockerFixture

from f4ratk.web.server import create_app


@fixture(scope='function')
def create_env(mocker: MockerFixture):
    def _create_env(new_env: Dict[str, str]) -> None:
        mocker.patch.dict(
            os.environ,
            new_env,
            clear=True,
        )

    return _create_env


@fixture(scope='function')
def app() -> Flask:
    yield create_app()


@fixture(scope='function')
def client(app: Flask) -> FlaskClient:
    app.config['TESTING'] = True
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True

    @app.after_request
    def after(response):
        print(response.status)
        print(response.headers)
        print(dumps(response.get_json(silent=True), indent=2))
        return response

    with app.test_client() as client:
        yield client
