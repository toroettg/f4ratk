##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from email.message import EmailMessage
from typing import Optional

from pytest import mark

from f4ratk.web.mail.mail import MailComposer, MailConfig, MailRequest


@mark.parametrize(
    "name,contact,expected",
    [
        ("John Doe", "john.doe@example.org", "John Doe (john.doe@example.org)"),
        ("John Doe", None, "John Doe"),
        (None, "john.doe@example.org", "john.doe@example.org"),
        (None, None, "Anonymous User"),
    ],
)
def given_varying_personal_information_when_composing_mail_should_adjust_subject(
    name: Optional[str], contact: Optional[str], expected: str
):
    composer = MailComposer(
        MailConfig(from_address="from@example.org", to_address="to@example.org")
    )

    mail: EmailMessage = composer.mail(
        MailRequest(name=name, contact=contact, content=None, bot=False)
    )

    assert expected in mail['Subject']
