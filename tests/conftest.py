##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from typing import Callable, Tuple, Union
from unittest.mock import Mock

from pandas import DataFrame, DatetimeIndex, MultiIndex, PeriodIndex
from pytest import fixture
from pytest_mock import MockerFixture
from requests import Session
from yahooquery import Ticker

from f4ratk.analyze.api import FundReturns
from f4ratk.fama import FamaReader
from f4ratk.file.reader import CsvFileReader

Quote = Return = Tuple[str, float]
QuotesFactory = FamaFactory = Callable[[Tuple[Return, ...]], DataFrame]
ReturnsIndex = Union[PeriodIndex, DatetimeIndex]
ReturnsFactory = Callable[[Tuple[Return, ...], ReturnsIndex], FundReturns]
CsvFileReaderFactory = Callable[[Tuple[Return, ...]], CsvFileReader]
YahooTickerReaderFactory = Callable[[str], Ticker]
YahooQueryTickerMockFixture = YahooQueryTickerPatchFixture = Callable[
    [Tuple[Quote, ...]], YahooTickerReaderFactory
]
FamaReaderFactory = Callable[[Tuple[Return, ...]], FamaReader]


@fixture(scope='module', name="create_fama")
def fama_factory() -> FamaFactory:
    def _fama_factory(*returns: Return):
        periods, returns = list(zip(*returns))
        data = DataFrame(
            data={
                'MKT': returns,
                'SMB': returns,
                'HML': returns,
                'RMW': returns,
                'CMA': returns,
                'WML': returns,
                'RF': returns,
            },
            index=PeriodIndex(data=periods, dtype='period[M]'),
        )

        return data

    return _fama_factory


@fixture(scope='module', name="create_quotes")
def quotes_factory() -> QuotesFactory:
    def _quotes_factory(symbol: str, *quotes: Quote):
        index_data, returns = list(zip(*quotes))

        def create_index() -> MultiIndex:
            return MultiIndex.from_tuples(
                tuples=tuple((symbol, date) for date in index_data),
                names=("symbol", "date"),
            )

        data = DataFrame(
            data={'adjclose': returns},
            index=create_index(),
        )

        return data

    return _quotes_factory


@fixture(scope='module', name="create_returns")
def returns_factory() -> ReturnsFactory:
    def _returns_factory(*returns: Return, index: ReturnsIndex = PeriodIndex):
        index_data, returns = list(zip(*returns))

        def create_index() -> ReturnsIndex:
            if index is PeriodIndex:
                return PeriodIndex(data=index_data, dtype='period[M]')
            elif index is DatetimeIndex:
                return DatetimeIndex(data=index_data, dtype='datetime64[ns]')

        data = DataFrame(
            data={'Returns': returns},
            index=create_index(),
        )

        return FundReturns(data=data)

    return _returns_factory


@fixture(scope='module', name="create_csv_file_reader")
def csv_file_reader_factory() -> CsvFileReaderFactory:
    def _csv_file_reader_factory(*returns: Return) -> CsvFileReader:
        index_data, returns = list(zip(*returns))

        data = DataFrame(
            data={'Returns': returns},
            index=DatetimeIndex(name="Dates", data=index_data, dtype='datetime64[ns]'),
        )

        return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

    return _csv_file_reader_factory


@fixture(scope='function', name="mock_yahoo_quotes")
def yahoo_query_ticker_mock_fixture(
    create_quotes: QuotesFactory,
) -> YahooQueryTickerMockFixture:
    def _yahoo_query_ticker_mock_fixture(*quotes: Quote) -> YahooTickerReaderFactory:
        def ticker_init_stub(symbol: str) -> Ticker:
            mock_reader = Mock(spec_set=Ticker)
            mock_reader.history.return_value = create_quotes(symbol, *quotes)
            return mock_reader

        return Mock(spec_set=Ticker, side_effect=ticker_init_stub)

    return _yahoo_query_ticker_mock_fixture


@fixture(scope='function', name="patch_yahoo_quotes")
def yahoo_query_ticker_patch_fixture(
    mocker: MockerFixture,
    create_quotes: QuotesFactory,
) -> YahooQueryTickerPatchFixture:
    def _yahoo_query_ticker_patch_fixture(*quotes: Quote) -> YahooTickerReaderFactory:
        def ticker_init_stub(symbol: str, session: Session) -> Ticker:
            mock_reader = Mock(spec_set=Ticker)
            mock_reader.history.return_value = create_quotes(symbol, *quotes)
            return mock_reader

        mocker.patch(
            'f4ratk.data_reader.Ticker', spec_set=Ticker, side_effect=ticker_init_stub
        )

    return _yahoo_query_ticker_patch_fixture


@fixture(scope='function', name="fama_returns")
def fama_reader_factory(
    mocker: MockerFixture, create_fama: FamaFactory
) -> FamaReaderFactory:
    def _fama_reader_factory(*returns: Return):
        returns = create_fama(*returns)
        mocker.patch(
            'f4ratk.infrastructure.FamaReader',
            spec_set=FamaReader,
            **{'fama_data.return_value': returns},
        )

    return _fama_reader_factory
