##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from typing import Callable
from unittest.mock import MagicMock

from pandas import DataFrame, Series
from pytest import approx, fixture

from f4ratk.analyze.evaluation import Critic
from f4ratk.analyze.regression import ModelType, Result
from f4ratk.history import AnnualizedReturns

SeriesFactory = Callable[..., Series]
HistoricFactory = Callable[..., DataFrame]


@fixture(scope='module', name="create_series")
def _series_factory() -> SeriesFactory:
    def _series_factory(
        MKT: float, SMB: float, HML: float, RMW: float, CMA: float, WML: float
    ):
        data = Series(
            data=[MKT, SMB, HML, RMW, CMA, WML],
            index=['MKT', 'SMB', 'HML', 'RMW', 'CMA', 'WML'],
        )

        return data

    return _series_factory


@fixture(scope='module', name="create_historic")
def _historic_factory(create_series: SeriesFactory) -> HistoricFactory:
    def _historic_factory(
        MKT: float, SMB: float, HML: float, RMW: float, CMA: float, WML: float
    ):
        return DataFrame(
            data=create_series(MKT=MKT, SMB=SMB, HML=HML, RMW=RMW, CMA=CMA, WML=WML),
            columns=['Returns'],
        )

    return _historic_factory


def given_any_data_when_run_should_sum_significant_loadings_as_factor_of_historic_returns(  # noqa: E501
    create_series: SeriesFactory, create_historic: HistoricFactory
):
    pvalues = create_series(MKT=0.005, SMB=0.01, HML=0.08, RMW=0.095, CMA=0.1, WML=0)
    params = create_series(MKT=1.02, SMB=0.86, HML=0.27, RMW=0.20, CMA=0.06, WML=1)
    historic = create_historic(MKT=5.09, SMB=2.35, HML=3.48, RMW=2.84, CMA=3.19, WML=2)

    result = Result(
        model_type=ModelType.FF6,
        model=MagicMock(pvalues=pvalues, **{'params.__getitem__.return_value': params}),
    )

    historic_returns = AnnualizedReturns(_data=historic, _first=None, _last=None)

    evaluated = Critic().evaluate(result=result, historic_returns=historic_returns)

    assert evaluated.evaluation == approx(2.815, rel=0.0001)


def given_any_data_when_run_should_normalize_mkt_factor_loading_to_base_at_1point0(  # noqa: E501
    create_series: SeriesFactory, create_historic: HistoricFactory
):
    pvalues = create_series(
        MKT=0.099, SMB=0.099, HML=0.099, RMW=0.099, CMA=0.099, WML=0.099
    )
    params = create_series(MKT=1.0, SMB=0, HML=0, RMW=0, CMA=0, WML=0)
    historic = create_historic(MKT=5.0, SMB=1, HML=1, RMW=1, CMA=1, WML=1)

    result = Result(
        model_type=ModelType.FF6,
        model=MagicMock(pvalues=pvalues, **{'params.__getitem__.return_value': params}),
    )

    historic_returns = AnnualizedReturns(_data=historic, _first=None, _last=None)

    evaluated = Critic().evaluate(result=result, historic_returns=historic_returns)

    assert evaluated.evaluation == 0


def given_any_data_when_run_should_evaluate_with_half_of_historic_factor_returns(  # noqa: E501
    create_series: SeriesFactory, create_historic: HistoricFactory
):
    pvalues = create_series(
        MKT=0.099, SMB=0.099, HML=0.099, RMW=0.099, CMA=0.099, WML=0.099
    )
    params = create_series(MKT=1.1, SMB=1, HML=1, RMW=1, CMA=1, WML=1)
    historic = create_historic(MKT=2, SMB=2, HML=2, RMW=2, CMA=2, WML=2)

    result = Result(
        model_type=ModelType.FF6,
        model=MagicMock(pvalues=pvalues, **{'params.__getitem__.return_value': params}),
    )

    historic_returns = AnnualizedReturns(_data=historic, _first=None, _last=None)

    evaluated = Critic().evaluate(result=result, historic_returns=historic_returns)

    assert evaluated.evaluation == 5.1


def given_any_data_when_run_should_ignore_factors_with_pvalue_equal_to_0point1_or_above(  # noqa: E501
    create_series: SeriesFactory, create_historic: HistoricFactory
):
    pvalues = create_series(
        MKT=1, SMB=0.099, HML=0.099, RMW=0.099, CMA=0.099, WML=0.099
    )
    params = create_series(MKT=1, SMB=1, HML=1, RMW=1, CMA=1, WML=1)
    historic = create_historic(MKT=5.0, SMB=2, HML=2, RMW=2, CMA=2, WML=2)

    result = Result(
        model_type=ModelType.FF6,
        model=MagicMock(pvalues=pvalues, **{'params.__getitem__.return_value': params}),
    )

    historic_returns = AnnualizedReturns(_data=historic, _first=None, _last=None)

    evaluated = Critic().evaluate(result=result, historic_returns=historic_returns)

    assert evaluated.evaluation == 5
