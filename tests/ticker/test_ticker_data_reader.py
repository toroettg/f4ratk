##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from pandas import PeriodDtype, PeriodIndex
from pandas.tseries.offsets import BusinessDay
from pytest import approx, raises

from f4ratk.analyze.api import FundReturns
from f4ratk.data_reader import yahoo_reader
from f4ratk.domain import Currency, Frame, Frequency
from f4ratk.shared import Downsampler, Normalizer
from f4ratk.ticker.api import NoTickerData
from f4ratk.ticker.reader import Stock, TickerReaderAdapter
from tests.conftest import YahooQueryTickerMockFixture


def given_any_ticker_symbol_when_reading_stock_data_should_return_data_with_business_daily_frequency():  # noqa: E501
    returns = TickerReaderAdapter(
        yahoo_reader=yahoo_reader, normalizer=Normalizer(), downsampler=None
    ).read_ticker(
        stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
        frame=Frame(frequency=Frequency.DAILY, start=None, end=None),
    )

    assert isinstance(returns, FundReturns)
    assert isinstance(returns.data.index, PeriodIndex)
    assert returns.data.index.dtype == PeriodDtype(freq=BusinessDay())


def given_any_ticker_symbol_when_reading_stock_data_should_convert_adjusted_close_to_returns_as_relative_percentage(  # noqa: E501
    mock_yahoo_quotes: YahooQueryTickerMockFixture,
):
    mock_reader = mock_yahoo_quotes(
        ('2020-04-01', 3.0),
        ('2020-04-02', 1.5),
        ('2020-04-03', 2.0),
    )

    returns = TickerReaderAdapter(
        yahoo_reader=mock_reader, normalizer=Normalizer(), downsampler=None
    ).read_ticker(
        stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
        frame=Frame(frequency=Frequency.DAILY, start=None, end=None),
    )

    assert returns.data['Returns']['2020-04-02'] == -50.0
    assert returns.data['Returns']['2020-04-03'] == approx(33.33333, rel=0.0001)


def given_monthly_frequency_and_daily_data_when_reading_ticker_symbol_should_interpolate_if_month_end_missing_and_resample_to_monthly(  # noqa: E501
    mock_yahoo_quotes: YahooQueryTickerMockFixture,
):
    mock_reader = mock_yahoo_quotes(
        ('2020-03-31', 3.75),
        ('2020-04-27', 3.0),
        ('2020-05-01', 1.5),
        ('2020-05-29', 2.0),
    )

    returns = TickerReaderAdapter(
        yahoo_reader=mock_reader, normalizer=Normalizer(), downsampler=Downsampler()
    ).read_ticker(
        stock=Stock(ticker_symbol='EUNL.DE', currency=Currency.USD),
        frame=Frame(frequency=Frequency.MONTHLY, start=None, end=None),
    )

    assert returns.data['Returns']['2020-04'] == -50.0
    assert returns.data['Returns']['2020-05'] == approx(6.66666, rel=0.0001)


def given_unknown_symbol_when_reading_ticker_symbol_should_raise_no_ticker_data_error(  # noqa: E501
    mock_yahoo_quotes: YahooQueryTickerMockFixture,
):
    mock_reader = mock_yahoo_quotes(
        # Intentionally Blank
    )

    with raises(NoTickerData):
        TickerReaderAdapter(
            yahoo_reader=mock_reader, normalizer=None, downsampler=None
        ).read_ticker(
            stock=Stock(ticker_symbol='UNKNOWN', currency=Currency.USD),
            frame=Frame(frequency=Frequency.MONTHLY, start=None, end=None),
        )
